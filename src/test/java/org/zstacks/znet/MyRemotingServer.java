package org.zstacks.znet;

import java.io.IOException;

import org.zstacks.znet.nio.Dispatcher;
import org.zstacks.znet.nio.Session;

public class MyRemotingServer extends RemotingServer{
	
	public MyRemotingServer(int port, Dispatcher dispatcher) throws IOException {  
		super(port, dispatcher);
		this.initHandlers();
	}
	
	//个性化定义Message中的那部分解释为命令，这里为了支持浏览器直接访问，把Message中的path理解为command
	@Override
	public String findHandlerKey(Message msg) { 
		String cmd = msg.getCommand();
		if(cmd == null){
			cmd = msg.getPath();
		}
		return cmd;
	}
	private void initHandlers(){
		//注册命令处理Callback
		this.registerHandler("hello", new MessageHandler() {
			public void handleMessage(Message msg, Session sess) throws IOException {
				//System.out.println(msg);
				msg.setStatus("200");   
				msg.setBody("hello world");
				sess.write(msg);
			}
		});
	}
	

	@SuppressWarnings("resource")
	public static void main(String[] args) throws Exception {   
		int selectorCount = Helper.option(args, "-selector", 1);
		int executorCount = Helper.option(args, "-executor", 128);
		
		Dispatcher dispatcher = new Dispatcher()
			.selectorCount(selectorCount)   //Selector线程数配置
			.executorCount(executorCount); //Message后台处理线程数配置
		
		MyRemotingServer server = new MyRemotingServer(80, dispatcher);
    	server.start();
		
    	//dispatcher.close(); 
	}
}
