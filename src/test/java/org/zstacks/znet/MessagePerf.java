package org.zstacks.znet;

import org.zstacks.znet.nio.IoBuffer;


public class MessagePerf {

	public static void main(String[] args) { 
		long start = System.currentTimeMillis();
		final int N = 1;   
		
		Message msg = new Message(); 
		msg.setMq("xx");
		msg.setMsgId("123456"); 
		msg.setHead("key1", "value1");
		msg.setHead("key2", "value2");
		msg.setHead("key3", "value3");
		msg.setHead("key4", "value4");
		msg.setHead("key5", "value5");
		msg.setBody("hello world");
		
		IoBuffer bb = msg.toIoBuffer();
		byte[] data = new byte[bb.remaining()];
		bb.duplicate().get(data);
		System.out.println(new String(data));
		
		MessageAdaptor ma = new MessageAdaptor();
		for(int i=0; i<N; i++){  
			IoBuffer bc = bb.duplicate();
			msg = (Message)ma.decode(bc);
			System.err.println(msg);
		}
		
		long end = System.currentTimeMillis();
		System.out.println(N*1000.0/(end-start));

	}

}
