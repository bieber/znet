package org.zstacks.znet.nio;



public class DispatcherTest {

	@SuppressWarnings("resource")
	public static void main(String[] args) throws Exception {
		
		Dispatcher dispatcher = new Dispatcher() 
			.selectorCount(2)
			.executorCount(4);
		
		dispatcher.start();
		dispatcher.close();
	} 
}
