package org.zstacks.znet;

import org.zstacks.znet.nio.Dispatcher;

public class MyRemotingClient {

	public static void main(String[] args) throws Exception { 
		
		//1)创建Dispatcher
		final Dispatcher dispatcher = new Dispatcher(); 
		//2)建立链接
		final RemotingClient client = new RemotingClient("127.0.0.1:80", dispatcher);
		
		Message msg = new Message();
		msg.setCommand("hello");
		msg.setHead("test", "测试");
		msg.setBody("hello world");
		
		Message res = client.invokeSync(msg);
		System.out.println(res);
		
		client.close();
		dispatcher.close();
	}
}
