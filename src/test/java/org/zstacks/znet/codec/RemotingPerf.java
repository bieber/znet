package org.zstacks.znet.codec;

import java.util.concurrent.atomic.AtomicLong;

import org.zstacks.znet.Helper;
import org.zstacks.znet.nio.Dispatcher;

class Task extends Thread{
	private final StringClient client;
	private final AtomicLong counter;
	private final long startTime;
	private final long N;
	public Task(StringClient client, AtomicLong counter, long startTime, long N) {
		this.client = client;
		this.counter = counter;
		this.startTime = startTime;
		this.N = N;
	}
	@Override
	public void run() { 
		for(int i=0; i<N; i++){ 
			try {
				client.invokeAsync("test");
				counter.incrementAndGet();
			} catch (Exception e) { 
				e.printStackTrace();
			}
			if(counter.get()%5000==0){
				double qps = counter.get()*1000.0/(System.currentTimeMillis()-startTime);
				System.out.format("QPS: %.2f\n", qps);
			}
		}
	}
}

public class RemotingPerf {
	
	@SuppressWarnings("resource")
	public static void main(String[] args) throws Exception {  
		int selectorCount = Helper.option(args, "-selector", 1);
		int executorCount = Helper.option(args, "-executor", 16);
		final long N = Helper.option(args, "-N", 1000000);
		final int threadCount =  Helper.option(args, "-thread", 100);
		final String serverAddress = Helper.option(args, "-s", "127.0.0.1:80");
		
		Dispatcher dispatcher = new Dispatcher()
				.selectorCount(selectorCount)
				.executorCount(executorCount);
		
		dispatcher.start();
	 
		final AtomicLong counter = new AtomicLong(0);
		
		StringClient[] clients = new StringClient[threadCount];
		for(int i=0;i<clients.length;i++){
			clients[i] = new StringClient(serverAddress, dispatcher);
		}
		
		final long startTime = System.currentTimeMillis();
		Task[] tasks = new Task[threadCount];
		for(int i=0; i<threadCount; i++){
			tasks[i] = new Task(clients[i], counter, startTime, N);
		}
		for(Task task : tasks){
			task.start();
		} 
		
		//4）释放链接资源与线程池相关资源
		//client.close();
		//dispatcher.close();
	} 
}
