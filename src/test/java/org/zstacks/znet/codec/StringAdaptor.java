package org.zstacks.znet.codec;

import org.zstacks.znet.nio.IoAdaptor;
import org.zstacks.znet.nio.IoBuffer;

public class StringAdaptor extends IoAdaptor{ 
	
	public IoBuffer encode(Object obj) { 
		if(!(obj instanceof String)){ 
			throw new RuntimeException("Message unknown"); 
		} 
		
		String msg = (String)obj;   
		byte[] b = msg.getBytes();
		IoBuffer buf = IoBuffer.allocate(4+b.length);
		buf.put(b.length);
		buf.put(b);
		
		buf.flip();
		return buf; 
	}
        
	public Object decode(IoBuffer buf) {  
		if(buf.remaining() < 4) return null;
		buf.mark();
		int len = buf.getInt();
		if(buf.remaining() < len){
			buf.reset();
			return null;
		}
		byte[] b = new byte[len];
		buf.get(b);
		return new String(b);
	}  
}