package org.zstacks.znet.usecase;


import java.io.IOException;

import org.zstacks.znet.Message;
import org.zstacks.znet.RemotingClient;
import org.zstacks.znet.nio.Dispatcher;
import org.zstacks.znet.ticket.ResultCallback;

public class AsyncClient {

	public static void main(String[] args) throws Exception { 
		final Dispatcher dispatcher = new Dispatcher();

		final RemotingClient client = new RemotingClient("127.0.0.1:80", dispatcher);
	
		Message msg = new Message();
		msg.setCommand("hello");
		msg.setBody("hello");
		//异步请求
		client.invokeAsync(msg, new ResultCallback() {
			
			@Override
			public void onCompleted(Message result) {
				System.out.println(result);
				try {
					client.close();
					dispatcher.close();
				} catch (IOException e) {
					//ignore
				} 
			}
		});
	}

}
