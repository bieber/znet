package org.zstacks.znet.log.impl;

import org.apache.log4j.Level;
import org.zstacks.znet.log.Logger;

public class Log4jLogger extends Logger { 
	private org.apache.log4j.Logger log;
	
	private static final String FQCN = Log4jLogger.class.getName();
	
	Log4jLogger(Class<?> clazz) {
		log = org.apache.log4j.Logger.getLogger(clazz);
	}
	
	Log4jLogger(String name) {
		log = org.apache.log4j.Logger.getLogger(name);
	}
	
	public void info(String message) {
		log.log(FQCN, Level.INFO, message, null);
	}
	
	public void info(String message, Throwable t) {
		log.log(FQCN, Level.INFO, message, t);
	}
	
	public void debug(String message) {
		log.log(FQCN, Level.DEBUG, message, null);
	}
	
	public void debug(String message, Throwable t) {
		log.log(FQCN, Level.DEBUG, message, t);
	}
	
	public void warn(String message) {
		log.log(FQCN, Level.WARN, message, null);
	}
	
	public void warn(String message, Throwable t) {
		log.log(FQCN, Level.WARN, message, t);
	}
	
	public void error(String message) {
		log.log(FQCN, Level.ERROR, message, null);
	}
	
	public void error(String message, Throwable t) {
		log.log(FQCN, Level.ERROR, message, t);
	}
	
	public void fatal(String message) {
		log.log(FQCN, Level.FATAL, message, null);
	}
	
	public void fatal(String message, Throwable t) {
		log.log(FQCN, Level.FATAL, message, t);
	}
	
	public boolean isDebugEnabled() {
		return log.isDebugEnabled();
	}
	
	public boolean isInfoEnabled() {
		return log.isInfoEnabled();
	}
	
	public boolean isWarnEnabled() {
		return log.isEnabledFor(Level.WARN);
	}
	
	public boolean isErrorEnabled() {
		return log.isEnabledFor(Level.ERROR);
	}
	
	public boolean isFatalEnabled() {
		return log.isEnabledFor(Level.FATAL);
	}
}

